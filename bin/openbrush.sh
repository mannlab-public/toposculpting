#!/bin/bash
function sendcom {
  host_name=192.168.50.114
  curl --get --data-urlencode "$1" \
    -v http://$host_name:40074/api/v1
}

%var brushes = ["Light","Icing","OilPaint","Ink","ThickPaint","WetPaint","Marker","TaperedMarker","PinchedMarker","Highlighter","Flat","TaperedFlat","PinchedFlat","SoftHighlighter","Fire","Embers","Smoke","Snow","Rainbow","Stars","VelvetInk","Waveform","Splatter","DuctTape","Paper","CoarseBristles","DrWigglez","Electricity","Streamers","Hypercolor","Bubbles","NeonPulse","CelVinyl","HyperGrid","LightWire","ChromaticWave","Dots","Petal","Toon","Wire","Spikes","Lofted","Disco","Comet","ShinyHull","MatteHull","UnlitHull","Diamond","Leaves","DotMarker","Plasma","Taffy","Flat"];

sendcom "new"
sendcom "brush.size.set=0.4"
%sendcom "brush.type=Icing"
sendcom "brush.type=Light"
sendcom "color.rgb=1.0,0.0,0.1"

