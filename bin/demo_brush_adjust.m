%test various brush widths, brushes go from 0.1 to 1.0
%but 0.0 seems discernible from 0.1

system("ob brush.forcepainting=false");
system("ob new");
system ("ob brush.forcepainting=true");
system ("ob brush.size.set=0.1");
system ("ob brush.type=Icing");
%brush sizes go from .1 to 1
for i=20:-1:0
  system("ob brush.forcepainting=false");
  %system("ob brush.size.add=0.1");
  system(sprintf("ob brush.size.set=%d",i/20));
  %system(sprintf("ob brush.size.set=%d",5));
  system("ob brush.forcepainting=true");
  system("sleep 2");
end%for
system("ob brush.forcepainting=false");

