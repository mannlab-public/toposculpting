#!/bin/bash

ob brush.forcepainting=false
ob new
ob brush.forcepainting=true
ob brush.size.set=0.1
ob brush.type=Icing
for i in {1..5}
do
  # echo $i
  ob brush.forcepainting=false
  ob brush.size.add=0.$i
  ob brush.forcepainting=true
  sleep 2
done

