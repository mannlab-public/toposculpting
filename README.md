# toposculpting

## Background:

About 10 years ago we started a project at Meta doing "Toposculpting"; read over toposculpting.pdf document <a href=docs/toposculpting_abaq.pdf>here</a> (also at http://wearcam.org/abaq.pdf) and look at some examples in http://wearcam.org/metabaq/

-------------
<img height=256 src=images/alexSWIM_closecrop.gif> <img height=256 src=images/Steve_and_Alex_wearing_Meta2_in_SteveMann_office_at_Meta_out900x.gif>

We're looking at OpenBrush to implement some of these ideas in a way that has more widespread deployment.  See also the eXtended metaVerse (XV), openxv.org and check out the research papers and video links there.

## Open Brush

#### Running Open Brush in Monoscopic Mode (PREFERRED AND RECOMMENDED METHOD FOR DEVELOPMENT AND LEARNING)
https://docs.openbrush.app/user-guide/monoscopic-mode
<br />
<br />
Download (your version based on Windows / Linux) and Unzip:
<br />
http://wearcam.org/abaq/openbrush/

##### Windows
<img src="images/monoscopic_mode.PNG" width="300" />
<br />
If you want the latest (recommended) Windows version of Monoscopic, go to the releases (new ones daily):
<br />
https://github.com/icosa-gallery/open-brush/releases
<br />
Click on the Latest Release and download OpenBrush_Mono_X.Y.Z.zip:
<br />
<img src="images/download_monoscopic.PNG" width="200" />

##### Linux and macOS
The latest Linux and macOS Monoscopic is only created on request, but can be done.
<br />
"Linux Monoscopic Experimental.zip" is running on Ubuntu:

```
chmod a+x OpenBrush-tempprexr2
./OpenBrush-tempprexr2 &
```

### Open Brush API (the real reason why we are even here)
https://docs.openbrush.app/user-guide/open-brush-api/api-commands
<br />
For YOUR version of the API running within Monoscopic (or headset) it is recommended to check locally:
http://localhost:40074/help/commands
<br />
https://docs.openbrush.app/user-guide/open-brush-api#whats-the-simplest-way-to-use-the-api
<br />

### Accessing Open Brush API in HEADSET when hostname != localhost
Ignore this if you're sending commands to Monoscopic App. However, if you access the Open Brush API web server running within the Quest headset and get a Forbidden, Access Denied, or 403 error, look below: <br />
https://docs.openbrush.app/user-guide/open-brush-api#how-do-i-configure-it

<img src="images/open_brush_cfg.PNG" width="300" />

### Confirm you can access Open Brush Web Server
http://localhost:40074/help

### Highly recommend exampleScripts to familiarize with the API
http://localhost:40074/examplescripts
<br />

<img src="images/example_scripts.JPG" width="400"/>


### Bash Setup
NOTE: You will need the following file which is referenced from within the Octave examples. The file is nothing more than a proxy for curl command to call the API with param=value pairs.
<br />

```
root@TABLET-6KNJUHBM:~/temp/git_repos/toposculpting# more bin/sendcom.sh
if [[ -z "${OB_HOST}" ]]; then
  host_name="localhost"
else
  host_name=${OB_HOST}
fi
curl --get --data-urlencode "$1" -v http://$host_name:40074/api/v1 

# COPY TO /usr/local/bin
root@TABLET:~/temp/git_repos/toposculpting# cp bin/sendcom.sh /usr/local/bin/sendcom.sh
root@TABLET:~/temp/git_repos/toposculpting#
root@TABLET:/usr/local/bin# ln -s sendcom.sh sendcom
root@TABLET:/usr/local/bin# ln -s sendcom.sh ob # "ob" is a preferrable shortcut to running commands, i.e. ob new

# MIGHT HELP TO HAVE BOTH SH AND NON SH VERSION...
root@DESKTOP:/usr/local/bin# ls -al /usr/local/bin/send*
-rwxr-xr-x 1 root root 71 Dec 13 02:28 /usr/local/bin/sendcom
-rwxr-xr-x 1 root root 71 Dec 13 02:20 /usr/local/bin/sendcom.sh


```

### ![Octave](/octave/README.md)

# ![](images/xvmannfun.mp4)

#### Running Open Brush on Quest 2
https://docs.openbrush.app/user-guide/get-started-with-open-brush


### Oculus Quest 2

Change "localhost" to your ip_address. <br />
To get your ip_address from Quest:

https://www.youtube.com/shorts/gL1vgWubcJw

<img src="images/quest2-ip-mac-address.jpg" width="400"/>


