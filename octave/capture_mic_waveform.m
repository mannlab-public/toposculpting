% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"

% https://docs.openbrush.app/user-guide/open-brush-api/api-commands
system("sendcom new");
%system("sendcom brush.type=Light"); % https://docs.openbrush.app/user-guide/brushes/brush-list
%system("sendcom brush.type=Diamond");
%system("sendcom color.rgb=1.0,0.0,0.5");
system("sendcom color.rgb=1.0,0.0,0.0");
system("sendcom brush.size.set=0.2");

f=3/4;
t=5*(0:255)/256;
%c=cos(2*pi*f*t)+1; % DC offset to not have negative y values
DCoffset=1;
scale=2;
%c=sin(2*pi*f*t)+DCoffset; % DC offset to not have negative y values
c=sin(2*pi*f*t)+1/3*sin(2*pi*3*f*t)+1/5*sin(2*pi*5*f*t); % DC offset to not have negative y values
c=c*scale + DCoffset;
s=cos(2*pi*f*t)+1/3*cos(2*pi*3*f*t)+1/5*cos(2*pi*5*f*t); % DC offset to not have negative y values
s=s*scale + DCoffset;
audio = audiorecorder();
record(audio);
pause(1);
#y = record(1, 500);
x0 = 0;
y0 = 0;
z0 = 0;

for n = 1:2000
  get(audio);
  data = getaudiodata(audio);
  y = 10 * data(end);
  x = x0 + 0.02;
  z = 0;
  system(sprintf("sendcom draw.path=[%d,%d,%d],[%d,%d,%d]",x0,y0,z0,x,y,z));
  system(sprintf("sendcom user.move.to=%d,%d,%d",-x,1,1))
  x0 = x;
  y0 = y;
  z0 = z;
end%for

stop(audio);
