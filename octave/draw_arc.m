% https://stackoverflow.com/questions/25594597/drawing-an-arc-in-octave
% draw_arc(0,pi,0,0,1)
function draw_arc(theta1, theta2, xc, yc, r);
  theta = linspace(theta1, theta2,20);
  x = r*cos(theta) + xc;
  y = r*sin(theta) + yc;
  draw_path=""
  for i=1:length(x)
    draw_path = strcat(draw_path, sprintf("[%d,%d,%d],",x(i),y(i),0));
  end
  disp(draw_path);
  system(sprintf("ob draw.path=%s", draw_path));

