% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"
% export OB_HOST=192.168.106.210

% SEE FULL API: http://localhost:40074/help/commands
% system("sendcom new");

system("sendcom brush.move.to=.5,21.8,0");
system("sendcom brush.turn.z=-45");
system("sendcom brush.size.set=1.4")
system("sendcom brush.type=Light"); % http://localhost:40074/help/brushes
%system("sendcom color.set.rgb=0.0,0.0,1");

function drawSquare(r)
  draw_path=""
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",r,r,0,-r,r,0));
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",-r,r,0,-r,-r,0));
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",-r,-r,0,r,-r,0));
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",r,-r,0,r,r,0));
  system(sprintf("sendcom draw.path=%s",draw_path));
endfunction

for r=1:10
 % use helper function
 drawSquare(r);
 system(sprintf("sendcom brush.move.by=0,0,-5")) % units are 10cm per number, e.g. 5 moves 50cm
 %printf("sendcom draw.polygon=4,%d,0",r) % alternative way to draw a square, but getting 'broken' corners
 system("sendcom color.add.hsv=%d,%d,0.0",0.1);
end%for

