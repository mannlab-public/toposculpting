% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"
% export OB_HOST=192.168.106.210

% SEE FULL API: http://localhost:40074/help/commands
% system("sendcom new");

system("sendcom brush.move.to=.5,17.3,0");
system("sendcom brush.turn.z=-45");
system("sendcom brush.size.set=1.4")
system("sendcom brush.type=Light"); % http://localhost:40074/help/brushes
%system("sendcom color.set.rgb=0.0,0.0,1");

function drawSquare(r)
  draw_path=""
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",r,r,0,-r,r,0));
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",-r,r,0,-r,-r,0));
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",-r,-r,0,r,-r,0));
  draw_path = strcat(draw_path, sprintf("[%d,%d,%d],[%d,%d,%d],",r,-r,0,r,r,0));
  system(sprintf("sendcom draw.path=%s",draw_path));
endfunction

for r=1:12 % some multiple of 3 or 4
 % use helper function
 system(sprintf("sendcom layer.add"))
 system(sprintf("sendcom layer.activate=%d",r))
 drawSquare(r);
 system(sprintf("sendcom brush.move.by=0,0,-5")) % units are 10cm per number, e.g. 5 moves 50cm
 %printf("sendcom draw.polygon=4,%d,0",r) % alternative way to draw a square, but getting 'broken' corners
 system("sendcom color.add.hsv=%d,%d,0.0",0.1);
 system(sprintf("sendcom layer.hide=%d",r))
end%for

l=1;
while true % inf loop
 for j=0:2
   system(sprintf("sendcom layer.show=%d",mod(l+j,12)))
 end%for
   system(sprintf("sleep .25"));
 for j=0:2
   system(sprintf("sendcom layer.hide=%d",mod(l+j,12)))
 end%for
 if l==12; l=0; end%if
 l=l+1
end%while 

