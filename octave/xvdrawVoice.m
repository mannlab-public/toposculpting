% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"

% SEE FULL API: http://localhost:40074/help/commands
system("sendcom new");
system("sendcom brush.type=Light"); %% http://localhost:40074/help/brushes
%system("sendcom brush.type=Diamond");
%system("sendcom color.rgb=1.0,0.0,0.5");
system("sendcom color.rgb=1.0,1.0,0.0");
system("sendcom brush.size.set=0.2");
system("sendcom viewonly.toggle");
system(sprintf("sendcom draw.text=ECE516"));
system(sprintf("sendcom user.move.to=%d,%d,%d",-7, 0, 3));

pause(3);


recorder = audiorecorder(16000, 8, 1);
record(recorder)
pause(0.2);

x0 = 7;
y0 = 0;
z0 = 0;

for n = 1:1000
  get(recorder)

  data = getaudiodata(recorder);

  y = 10*data(end);
  x = x0 + 0.1;
  z = 0;
  system(sprintf("sendcom draw.path=[%d,%d,%d],[%d,%d,%d]",x0,y0,z0,x,y,z));
  system(sprintf("sendcom user.move.to=%d,%d,%d",-x, 0, 3));

  x0 = x;
  y0 = y;
end

for n = 1:20
  x = x0 + 0.1;
  system(sprintf("sendcom user.move.to=%d,%d,%d",-x, 0, 3));
  x0 = x;
end

