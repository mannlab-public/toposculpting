% NOTE: see https://gitlab.com/mannlab-public/toposculpting/-/tree/main/README.md about "sendcom"

% SEE FULL API: http://localhost:40074/help/commands
system("sendcom new");
system("sendcom brush.move.to=0,0,0");
system("sendcom brush.size.set=0.1")
system("sendcom brush.type=Light"); % http://localhost:40074/help/brushes
system("sendcom color.rgb=1.0,0.0,0.5");

% FOR PUTTING THE "DRAWING" INTO CENTER IN MONOSCOPIC MODE
system("sendcom user.move.to=-7,10,10")

f=3/4;
t=5*(0:127)/128;
%c=cos(2*pi*f*t)+1; % DC offset to not have negative y values
c=sin(2*pi*f*t);
c=sin(2*pi*f*t)+1/3*sin(2*pi*3*f*t)+1/5*sin(2*pi*5*f*t);
%plot(t,c);

%%%%%%%%%%%%%%%%%%%%
% USED TO MOVE THE ENTIRE PLOT AROUND
%%%%%%%%%%%%%%%%%%%%
X_OFFSET=0.5;
Y_OFFSET=1.5;

for n = 1:127
  system(sprintf("sendcom draw.path=[%d,%d,0],[%d,%d,0]",t(n)+X_OFFSET,c(n)+Y_OFFSET,t(n+1)+X_OFFSET,c(n+1)+Y_OFFSET));
end

Y_OFFSET=3.5;
draw_path = "";
for n = 1:127
  dp = sprintf("[%d,%d,0],",t(n)+X_OFFSET,c(n)+Y_OFFSET);
  draw_path = strcat(draw_path, dp);
end

% printf(draw_path)
system(sprintf("sendcom draw.path=%s", draw_path))

