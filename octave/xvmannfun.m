% SEE FULL API: http://localhost:40074/help/commands
system("sendcom new");
system("sendcom brush.type=Light"); % http://localhost:40074/help/brushes
%system("sendcom color.rgb=1.0,0.0,0.5");
system("sendcom color.rgb=1.0,0.0,0.0");
system("sendcom brush.size.set=0.2");

% FOR PUTTING THE "DRAWING" INTO CENTER IN MONOSCOPIC MODE
system("sendcom user.move.to=0,10,7")

ITERATIONS=255
f=3/4;
t=5*(0:(ITERATIONS-1))/ITERATIONS;
%c=cos(2*pi*f*t)+1; % DC offset to not have negative y values

SCALE=2;
c=sin(2*pi*f*t)+1/3*sin(2*pi*3*f*t)+1/5*sin(2*pi*5*f*t);
c=c*SCALE;
s=cos(2*pi*f*t)+1/3*cos(2*pi*3*f*t)+1/5*cos(2*pi*5*f*t);
s=s*SCALE;

%%%%%%%%%%%%%%%%%%%%
% USED TO MOVE THE ENTIRE PLOT AROUND
%%%%%%%%%%%%%%%%%%%%
X_OFFSET=0;
Y_OFFSET=5;

draw_path = "draw.path=";
for n = 1:(ITERATIONS-1)
  dp = sprintf("[%d,%d,%d],",s(n)+X_OFFSET,c(n)+Y_OFFSET,t(n));
  tmp = strcat(draw_path, dp);
  draw_path = tmp;
end

system(sprintf("sendcom %s", draw_path))