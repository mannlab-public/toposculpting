# octave

Many are running Ubuntu on WSL (Windows Subsystem for Linux). Which is like developing a vaccine on the virus. <br />
<br />
Installing Octave on Ubuntu: <br />
https://blog.eldernode.com/install-gnu-octave-on-ubuntu-20-04/

```
root@DESKTOP:~/temp/git_repos/toposculpting# cd octave
root@DESKTOP:~/temp/git_repos/toposculpting/octave# octave-cli xvmannfun.m
```
<img src="images/wave_1.JPG" width="600" />

# ![](/images/xvmannfun.mp4)

```
root@DESKTOP:~/temp/git_repos/toposculpting/octave# octave-cli xvsine.m
```
<img src="images/xvsine.JPG" width="600" />

```
root@DESKTOP:~/temp/git_repos/toposculpting/octave# octave-cli metavisionsquares_main.m
```
-------------
<img src="images/metavisionsquares_main.JPG" width="400" /> |

___

### Exercises

#### Example 1

```
sendcom new
sendcom brush.move.to=0.1,8,0
# COPY media/models/violin.glb TO YOUR "C:\Users\YOUR_USERNAME\AppData\LocalLow\Icosa\Open Brush\Media Library\Models" folder
sendcom import.model=violin.glb
sendcom user.move.to=-4,13,7
cd octave; octave-cli xvsine.m
sendcom user.move.to=-4,13,7
```

# ![](images/xv_violin.mp4)
-------------
# ![](images/arcs.mp4)

